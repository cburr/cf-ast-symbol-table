import json
from datetime import datetime, timedelta

import pytest

from . import do_put


def test_create_and_update(client):
    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404

    body = json.dumps({"hello": "world"})
    response = do_put(client, "/api/v0/symbol_table/conda", body)
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == ["conda"]

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = do_put(client, "/api/v0/symbol_table/conda", "null")
    assert response.status_code == 201

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() is None


@pytest.mark.parametrize("a,b", [("0", "1"), ("9", "10")])
def test_latest(client, a, b):
    response = client.get(f"/api/v{a}/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get(f"/api/v{a}/symbol_table/conda")
    assert response.status_code == 404

    response = client.get("/api/latest/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/latest/symbol_table/conda")
    assert response.status_code == 404

    # Put version 1
    body = json.dumps({"hello": "world"})
    response = do_put(client, f"/api/v{a}/symbol_table/conda", body)
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(f"/api/v{a}/symbol_table/")
    assert response.status_code == 200
    assert response.json() == ["conda"]

    response = client.get(f"/api/v{a}/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get("/api/latest/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    # Put version 2
    response = do_put(client, f"/api/v{b}/symbol_table/conda", "null")
    assert response.status_code == 201

    # Check both links point to the correct data
    response = client.get(f"/api/v{a}/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(f"/api/v{b}/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() is None

    response = client.get("/api/latest/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() is None

    response = client.get(f"/api/v{a}/symbol_table/")
    assert response.status_code == 200
    assert response.json() == ["conda"]

    response = client.get(f"/api/v{b}/symbol_table/")
    assert response.status_code == 200
    assert response.json() == ["conda"]

    response = client.get("/api/latest/symbol_table/")
    assert response.status_code == 200
    assert response.json() == ["conda"]


def test_multiple_with_dots(client):
    body = json.dumps({"hello": "aaa"})
    response = do_put(client, "/api/v0/symbol_table/backports.aaa", body)
    assert response.status_code == 201
    assert response.json() is True

    body = json.dumps({"hello": "bbb"})
    response = do_put(client, "/api/v0/symbol_table/backports.bbb", body)
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == ["backports.aaa", "backports.bbb"]


def test_missing_signature_new(client):
    response = client.put("/api/v0/symbol_table/conda", data="null")
    assert response.status_code == 401

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404


def test_missing_signature_update(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", json.dumps({"hello": "world"})
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.put(
        "/api/v0/symbol_table/conda",
        data="null",
    )
    assert response.status_code == 401

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}


def test_invalid_signature_new(client):
    response = client.put(
        "/api/v0/symbol_table/conda",
        data="null",
        headers={
            "X-Headers-Signature": "xxxxx",
            "X-Signature-Timestamp": datetime.utcnow().isoformat(),
            "X-Body-Signature": "yyyyy",
        },
    )
    assert response.status_code == 401
    assert "Could not validate credentials" in response.text

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404


def test_invalid_signature_update(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", json.dumps({"hello": "world"})
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.put(
        "/api/v0/symbol_table/conda",
        data="null",
        headers={
            "X-Headers-Signature": "xxxxx",
            "X-Signature-Timestamp": datetime.utcnow().isoformat(),
            "X-Body-Signature": "yyyyy",
        },
    )
    assert response.status_code == 401
    assert "Could not validate credentials" in response.text

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}


def test_invalid_body_signature_new(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", "null", body_signature="xxxx"
    )
    assert response.status_code == 401
    assert "Could not validate credentials" in response.text

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404


def test_invalid_body_signature_update(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", json.dumps({"hello": "world"})
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = do_put(
        client, "/api/v0/symbol_table/conda", "null", body_signature="xxxx"
    )
    assert response.status_code == 401
    assert "Could not validate credentials" in response.text

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}


def test_invalid_timestamp_new(client):
    response = do_put(
        client,
        "/api/v0/symbol_table/conda",
        "null",
        timestamp=(datetime.utcnow() - timedelta(days=1)).isoformat(),
    )
    assert response.status_code == 401
    assert "Signature has expired" in response.text

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404


def test_invalid_timestamp_update(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", json.dumps({"hello": "world"})
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = do_put(
        client,
        "/api/v0/symbol_table/conda",
        "null",
        timestamp=(datetime.utcnow() - timedelta(days=1)).isoformat(),
    )
    assert response.status_code == 401
    assert "Signature has expired" in response.text

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}


def test_missing_body_new(client):
    response = do_put(client, "/api/v0/symbol_table/conda", "")
    assert response.status_code == 400

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404


def test_missing_body_update(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", json.dumps({"hello": "world"})
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = do_put(client, "/api/v0/symbol_table/conda", "")
    assert response.status_code == 400

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}


def test_invalid_body_new(client):
    # FIXME: Disabled as validating the JSON uses a lot of memory
    # response = client.put(
    #     "/api/v0/symbol_table/conda",
    #     data="invalid",
    #     headers={
    #         "X-Signature": "7529825ccf6ea5af218b2685e5e31dc4faaa1d6e34bc775acf2d66fcd5832b4f"  # noqa
    #     },
    # )
    # assert response.status_code == 400
    # assert response.json() == {"detail": "Invalid JSON in request body"}

    response = client.get("/api/v0/symbol_table/")
    assert response.status_code == 200
    assert response.json() == []

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 404


def test_invalid_body_update(client):
    response = do_put(
        client, "/api/v0/symbol_table/conda", json.dumps({"hello": "world"})
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    # FIXME: Disabled as validating the JSON uses a lot of memory
    # response = client.put(
    #     "/api/v0/symbol_table/conda",
    #     data="invalid",
    #     headers={
    #         "X-Signature": "7529825ccf6ea5af218b2685e5e31dc4faaa1d6e34bc775acf2d66fcd5832b4f"  # noqa
    #     },
    # )
    # assert response.status_code == 400
    # assert response.json() == {"detail": "Invalid JSON in request body"}

    response = client.get("/api/v0/symbol_table/conda")
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}
