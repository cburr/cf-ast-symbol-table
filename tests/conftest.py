import os
import pathlib

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import func

STORAGE_TYPES = [
    ["ZstandardStorage", None],
    ["CachedZstandardStorage", None],
    ["CachedZstandardStorage", "ASTST_TEST_PSQL_URL"],
    ["CachedZstandardStorage", "ASTST_TEST_MYSQL_URL"],
    ["DBZstandardStorage", None],
    ["DBZstandardStorage", "ASTST_TEST_PSQL_URL"],
    ["DBZstandardStorage", "ASTST_TEST_MYSQL_URL"],
]


@pytest.fixture(scope="function", params=STORAGE_TYPES)
def client(monkeypatch, tmpdir, request):
    # Allow the connection URL to be passed as an environment variable so
    # databases like postgresql and mysql can be tested
    db_class, db_url_env_var = request.param
    if db_url_env_var:
        db_url = os.environ.get(db_url_env_var)
        if not db_url:
            raise pytest.skip(f"Environment variable {db_url_env_var!r} is not set")
    else:
        db_url = None

    monkeypatch.setenv("ASTST_SIGNATURE_SECRET", "I-AM-NOT-A-SECRET")
    monkeypatch.setenv("ASTST_DATA_DIR", str(tmpdir))
    monkeypatch.setenv("ASTST_STORAGE_CLASS", "ZstandardStorage")
    import cf_ast_symbol_table

    cf_ast_symbol_table.config.data_dir = pathlib.Path(tmpdir)
    cf_ast_symbol_table.config.db_url = db_url
    cf_ast_symbol_table.store = getattr(cf_ast_symbol_table.storage, db_class)(
        pathlib.Path(tmpdir)
    )
    if hasattr(cf_ast_symbol_table.store, "files"):
        (count,) = cf_ast_symbol_table.store.conn.execute(
            func.count(cf_ast_symbol_table.store.files.c.id)
        ).first()
        if count > 0:
            raise RuntimeError(
                "The files table already exists, exiting to prevent possible data loss!"
            )

    yield TestClient(cf_ast_symbol_table.app)

    # Clean up the files table if needed
    if hasattr(cf_ast_symbol_table.store, "files"):
        cf_ast_symbol_table.store.files.drop(cf_ast_symbol_table.store.conn)
