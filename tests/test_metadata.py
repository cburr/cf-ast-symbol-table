import json

from . import do_put


def test_symbols_create_and_update(client):
    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        json.dumps({"hello": "world"}),
        metadata={"version": 0, "imports": ["hello"]},
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0/metadata"
    )
    assert response.status_code == 200
    assert response.json() == {"version": 0, "imports": ["hello"]}

    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        json.dumps({"hello": "world"}),
        metadata={"version": 0, "imports": ["hello", "world"]},
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0/metadata"
    )
    assert response.status_code == 200
    assert response.json() == {"version": 0, "imports": ["hello", "world"]}


def test_symbol_table_create_and_update(client):
    response = do_put(
        client,
        "/api/v0/symbol_table/conda",
        json.dumps({"hello": "world"}),
        metadata={"version": 0, "indexed artifacts": ["a/b/c"]},
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda/metadata")
    assert response.status_code == 200
    assert response.json() == {"version": 0, "indexed artifacts": ["a/b/c"]}

    response = do_put(
        client,
        "/api/v0/symbol_table/conda",
        json.dumps({"hello": "world"}),
        metadata={"version": 0, "indexed artifacts": ["a/b/c", "x/y/z"]},
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbol_table/conda/metadata")
    assert response.status_code == 200
    assert response.json() == {"version": 0, "indexed artifacts": ["a/b/c", "x/y/z"]}
