import json
from io import BytesIO

import zstandard

from . import do_put


def test_symbols_versioned(client):
    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        json.dumps({"hello": "world"}),
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0/json"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0/zstd"
    )
    assert response.status_code == 200
    data = BytesIO(response.content)
    with zstandard.ZstdDecompressor().stream_reader(data) as reader:
        data = reader.read()
    assert json.loads(data) == {"hello": "world"}


def test_symbols_latest(client):
    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        json.dumps({"hello": "world"}),
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0/json"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0/zstd"
    )
    assert response.status_code == 200
    data = BytesIO(response.content)
    with zstandard.ZstdDecompressor().stream_reader(data) as reader:
        data = reader.read()
    assert json.loads(data) == {"hello": "world"}
