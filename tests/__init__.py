import hashlib
import hmac
import json
from datetime import datetime
from typing import Any, Dict, Optional


def do_put(
    client,
    url: str,
    body: str,
    *,
    timestamp: Optional[str] = None,
    body_signature: Optional[str] = None,
    metadata: Optional[Dict[str, Any]] = None
):
    from cf_ast_symbol_table import config

    if timestamp is None:
        timestamp = datetime.utcnow().isoformat()

    if body_signature is None:
        body_signature = hmac.new(
            config.signature_secret, body.encode(), hashlib.sha256
        ).hexdigest()

    headers_signature = hmac.new(
        config.signature_secret,
        b"".join([url.encode(), timestamp.encode(), body_signature.encode()]),
        hashlib.sha256,
    )
    if metadata:
        metadata = json.dumps(metadata)
        headers_signature.update(metadata.encode())
    headers_signature = headers_signature.hexdigest()

    headers = {
        "X-Headers-Signature": headers_signature,
        "X-Signature-Timestamp": timestamp,
        "X-Body-Signature": body_signature,
    }

    return client.put(url, data=body, headers=headers, params=dict(metadata=metadata))
