import json

from . import do_put


def test_create_and_update(client):
    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 404

    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        json.dumps({"hello": "world"}),
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        "null",
    )
    assert response.status_code == 201

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() is None


def test_latest(client):
    response = client.get(
        "/api/v1/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 404

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 404

    # Put version 1
    response = do_put(
        client,
        "/api/v1/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        json.dumps({"hello": "world"}),
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/v1/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    # Put version 2
    response = client.get(
        "/api/v2/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 404

    response = do_put(
        client,
        "/api/v2/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0",
        "null",
    )
    assert response.status_code == 201

    # Check both links point to the correct data
    response = client.get(
        "/api/v1/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world"}

    response = client.get(
        "/api/v2/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() is None

    response = client.get(
        "/api/latest/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_0"
    )
    assert response.status_code == 200
    assert response.json() is None


def test_multiple_versions(client):
    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_1",
        json.dumps({"hello": "world1"}),
    )
    assert response.status_code == 201
    assert response.json() is True

    response = do_put(
        client,
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_2",
        json.dumps({"hello": "world2"}),
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_1"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world1"}

    response = client.get(
        "/api/v0/symbols/conda/conda-forge/noarch/conda-1.0.0-pyh012345_2"
    )
    assert response.status_code == 200
    assert response.json() == {"hello": "world2"}


def test_listing(client):
    response = client.get("/api/v0/symbols/")
    assert response.status_code == 200
    assert response.json() == []

    body = json.dumps({"hello": "aaa"})
    response = do_put(
        client,
        "/api/v0/symbols/backports.aaa/conda-forge/linux-64/backports.aaa-0.0.0-pyh012345_1",  # noqa
        body,
    )
    assert response.status_code == 201
    assert response.json() is True

    body = json.dumps({"hello": "bbb"})
    response = do_put(
        client,
        "/api/v0/symbols/backports.bbb/conda-forge/linux-64/backports.bbb-0.0.0-pyh012345_1",  # noqa
        body,
    )
    assert response.status_code == 201
    assert response.json() is True

    response = client.get("/api/v0/symbols/")
    assert response.status_code == 200
    assert set(response.json()) == {
        "backports.aaa/conda-forge/linux-64/backports.aaa-0.0.0-pyh012345_1",
        "backports.bbb/conda-forge/linux-64/backports.bbb-0.0.0-pyh012345_1",
    }
