FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

COPY . /src
RUN pip install /src

EXPOSE 8000
CMD ["uvicorn", "cf_ast_symbol_table:app", "--host=0.0.0.0", "--port=8000"]
