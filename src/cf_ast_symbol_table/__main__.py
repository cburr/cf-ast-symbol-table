import hashlib
import hmac
import json
import os
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime

import requests
import typer
from tqdm import tqdm

from cf_ast_symbol_table import store

app = typer.Typer()


@app.command()
def populate_db_http(
    version: int,
    source_host: str = "https://cf-ast-symbol-table.web.cern.ch",
    dest_host: str = "http://localhost:8000",
):
    secret_token = os.environ["ASTST_SIGNATURE_SECRET"].encode("utf-8")
    for suffix in ["symbol_table", "symbols"]:
        endpoint = f"/api/v{version}/{suffix}"
        typer.echo(
            f"Populating DB with from {source_host} to {dest_host} for {endpoint}"
        )
        r = requests.get(f"{source_host}{endpoint}")
        r.raise_for_status()
        with ThreadPoolExecutor(5) as pool:
            keys = r.json()
            with tqdm(total=len(keys)) as pbar:
                futures = []
                for key in keys:
                    futures += [
                        pool.submit(
                            retry_func,
                            copy_http,
                            source_host,
                            dest_host,
                            f"{endpoint}/{key}",
                            secret_token,
                        )
                    ]
                for _ in as_completed(futures):
                    pbar.update(1)


@app.command()
def populate_db_direct(
    version: int,
    source_host: str = "https://cf-ast-symbol-table.web.cern.ch",
):
    for suffix in ["symbol_table", "symbols"]:
        url = f"/api/v{version}/{suffix}"
        typer.echo(
            f"Directly populating DB with from {source_host} to {store!r} for {url}"
        )
        r = requests.get(f"{source_host}{url}")
        r.raise_for_status()
        with ThreadPoolExecutor(5) as pool:
            keys = r.json()
            with tqdm(total=len(keys)) as pbar:
                futures = []
                for key in keys:
                    futures += [
                        pool.submit(
                            retry_func, copy_direct, source_host, f"{url}/{key}"
                        )
                    ]
                for _ in as_completed(futures):
                    pbar.update(1)


def retry_func(func, *args, retries=3):
    try:
        func(*args)
    except Exception as e:  # pylint: disable=broad-except
        print(f"Error calling {func!r}(*{args!r}): {e!r}")
        if retries > 0:
            retry_func(func, *args, retries=retries - 1)


def copy_direct(source_host, url):
    assert url.startswith("/api/v"), url
    # FIXME: There is a potential race here if the data is updated after the
    # metadata is aquired. For now, get the metadata first to ensure we don't
    # think packages have been processed that aren't included in the data
    # Get the metadata
    r = requests.get(f"{source_host}{url}/metadata")
    r.raise_for_status()
    metadata = r.json()
    # Get the compressed data
    r = requests.get(f"{source_host}{url}/zstd")
    r.raise_for_status()
    data = r.content
    # Write to the storage
    store.write_bytes(url[len("/api/") :], metadata, data)


def copy_http(source_host, dest_host, url, secret_token):
    # FIXME: There is a potential race here if the data is updated after the
    # metadata is aquired. For now, get the metadata first to ensure we don't
    # think packages have been processed that aren't included in the data
    # Get the metadata
    r = requests.get(f"{source_host}{url}/metadata")
    r.raise_for_status()
    metadata = json.dumps(r.json())
    # Get the actual data
    r = requests.get(f"{source_host}{url}")
    r.raise_for_status()
    data = json.dumps(r.json())

    # Generate the signature
    headers = {
        "X-Signature-Timestamp": datetime.utcnow().isoformat(),
        "X-Body-Signature": hmac.new(
            secret_token, data.encode(), hashlib.sha256
        ).hexdigest(),
    }
    headers["X-Headers-Signature"] = hmac.new(
        secret_token,
        b"".join(
            [
                url.encode(),
                headers["X-Signature-Timestamp"].encode(),
                headers["X-Body-Signature"].encode(),
                metadata.encode(),
            ]
        ),
        hashlib.sha256,
    ).hexdigest()

    # Upload the data
    r = requests.put(
        f"{dest_host}{url}",
        data=data,
        headers=headers,
        params=dict(metadata=metadata),
    )
    r.raise_for_status()


if __name__ == "__main__":
    app()
