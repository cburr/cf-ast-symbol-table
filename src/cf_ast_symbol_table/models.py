from dataclasses import dataclass
from enum import Enum
from typing import Any, Dict


@dataclass
class UpdatePayload:
    metadata: Dict[str, Any]
    data: bytes


class ReturnFormat(str, Enum):
    JSON = "json"
    ZSTD = "zstd"
    METADATA = "metadata"
