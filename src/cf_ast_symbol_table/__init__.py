import hashlib
import hmac
import importlib.metadata
import json
from datetime import datetime, timedelta
from typing import Optional

from dateutil.parser import parse as date_parse
from fastapi import Depends, FastAPI, Header, Request, status
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.responses import StreamingResponse

from . import config, errors
from . import storage as _storage
from .logging import logger
from .models import ReturnFormat, UpdatePayload

try:
    __version__ = importlib.metadata.version(__name__)
except importlib.metadata.PackageNotFoundError:  # pragma: no cover
    __version__ = None  # pragma: no cover

store = getattr(_storage, config.storage_class)(config.data_dir)
app = FastAPI()
app.add_middleware(GZipMiddleware, minimum_size=500)

responses = {status.HTTP_404_NOT_FOUND: {"description": "The item was not found"}}


async def validate_headers(
    request: Request,
    x_headers_signature: str = Header(None),
    x_signature_timestamp: str = Header(None),
    x_body_signature: str = Header(None),
    metadata: Optional[str] = None,
):
    logger.info(
        "Received update payload sent at %s with signatures (%s, %s)",
        x_signature_timestamp,
        x_headers_signature,
        x_body_signature,
    )

    if not x_headers_signature or not x_signature_timestamp or not x_body_signature:
        raise errors.MISSING_CREDENTIALS

    # Validate the headers are signed correctly before parsing the body to make
    # denial of service attacks harder
    actual_signature = hmac.new(config.signature_secret, b"", hashlib.sha256)
    actual_signature.update(request.url.path.encode("utf-8"))
    actual_signature.update(x_signature_timestamp.encode("utf-8"))
    actual_signature.update(x_body_signature.encode("utf-8"))
    if metadata:
        actual_signature.update(metadata.encode("utf-8"))
    actual_signature = actual_signature.hexdigest()
    if not hmac.compare_digest(x_headers_signature, actual_signature):
        logger.error(
            "Invalid signature for request, expected %s got %s",
            actual_signature,
            x_headers_signature,
        )
        raise errors.INVALID_CREDENTIALS

    # Ensure the signature is recent
    if abs(datetime.utcnow() - date_parse(x_signature_timestamp)) > timedelta(hours=1):
        logger.warning(
            "Expired signature: Was signed at %s but the current time is %s",
            x_signature_timestamp,
            datetime.utcnow(),
        )
        raise errors.EXPIRED_SIGNATURE

    # The body's signature will be validated in validate_credentials
    return x_body_signature


async def validate_credentials(
    request: Request,
    body_signature: str = Depends(validate_headers),
    metadata: Optional[str] = None,
):
    signature, original_size, data = await store.request_to_signed_bytes(request)
    if original_size == 0:
        raise errors.MISSING_BODY
    logger.debug(
        f"Compressed payload for {request.url.path} from {original_size} to "
        f"{len(data)} ({len(data)/original_size:.2%})"
    )

    if not hmac.compare_digest(body_signature, signature):
        logger.error(
            f"Invalid signature for request body, expected {signature} "
            f"got {body_signature}"
        )
        raise errors.INVALID_CREDENTIALS

    if metadata:
        try:
            metadata = json.loads(metadata)
        except Exception as e:
            raise errors.INVALID_JSON_METADATA from e

    return UpdatePayload(metadata=metadata, data=data)


@app.get("/health/")
async def health():
    return {
        "status": "pass",
        "version": "1",
        "releaseId": __version__,
    }


@app.get("/api/latest/symbol_table/")
async def list_known_symbol_tables_latest():
    result = {x.split("/")[-1] async for x in store.glob("v*/symbol_table/*")}
    return sorted(result)


@app.get("/api/v{version}/symbol_table/")
async def list_known_symbol_tables(version: int):
    return sorted(
        [x.split("/")[-1] async for x in store.glob(f"v{version}/symbol_table/*")]
    )


@app.get("/api/latest/symbol_table/{pkg_name}", responses=responses)
@app.get("/api/latest/symbol_table/{pkg_name}/{data_format}", responses=responses)
async def symbol_table_latest(
    pkg_name: str, data_format: ReturnFormat = ReturnFormat.JSON
):
    try:
        name = await store.find_latest(
            f"symbol_table/{pkg_name}",
        )
    except KeyError as e:
        raise errors.NOT_FOUND from e
    return store.stream_object(data_format, name)


@app.get("/api/v{version}/symbol_table/{pkg_name}", responses=responses)
@app.get("/api/v{version}/symbol_table/{pkg_name}/{data_format}", responses=responses)
def symbol_table(
    pkg_name: str, version: int, data_format: ReturnFormat = ReturnFormat.JSON
):
    name = f"v{version}/symbol_table/{pkg_name}"
    if name not in store:
        raise errors.NOT_FOUND
    return store.stream_object(data_format, name)


@app.put(
    "/api/v{version}/symbol_table/{pkg_name}",
    status_code=201,
    responses={status.HTTP_400_BAD_REQUEST: {"description": "The item was not found"}},
)
async def update_symbol_table(
    pkg_name: str,
    version: int,
    payload: UpdatePayload = Depends(validate_credentials),
):
    store.write_bytes(
        f"v{version}/symbol_table/{pkg_name}", payload.metadata, payload.data
    )
    return True


@app.get("/api/v{version}/symbols/")
async def list_known_symbols(version: int):
    return StreamingResponse(
        store.streaming_json_glob(f"v{version}/symbols/", "*/*/*/*"),
        media_type="application/json",
    )


@app.get(
    "/api/latest/symbols/{pkg_name}/{channel}/{subdir}/{filename}/", responses=responses
)
@app.get(
    "/api/latest/symbols/{pkg_name}/{channel}/{subdir}/{filename}/{data_format}",
    responses=responses,
)
async def package_symbols_latest(
    pkg_name: str,
    channel: str,
    subdir: str,
    filename: str,
    data_format: ReturnFormat = ReturnFormat.JSON,
):
    try:
        name = await store.find_latest(
            f"symbols/{pkg_name}/{channel}/{subdir}/{filename}"
        )
    except KeyError as e:
        raise errors.NOT_FOUND from e
    return store.stream_object(data_format, name)


@app.get(
    "/api/v{version}/symbols/{pkg_name}/{channel}/{subdir}/{filename}",
    responses=responses,
)
@app.get(
    "/api/v{version}/symbols/{pkg_name}/{channel}/{subdir}/{filename}/{data_format}",
    responses=responses,
)
async def package_symbols(
    pkg_name: str,
    version: int,
    channel: str,
    subdir: str,
    filename: str,
    data_format: ReturnFormat = ReturnFormat.JSON,
):
    name = f"v{version}/symbols/{pkg_name}/{channel}/{subdir}/{filename}"
    if name not in store:
        raise errors.NOT_FOUND
    return store.stream_object(data_format, name)


@app.put(
    "/api/v{version}/symbols/{pkg_name}/{channel}/{subdir}/{filename}",
    status_code=201,
    responses={status.HTTP_400_BAD_REQUEST: {"description": "The item was not found"}},
)
async def update_package_symbols(
    pkg_name: str,
    channel: str,
    subdir: str,
    filename: str,
    version: int,
    payload: UpdatePayload = Depends(validate_credentials),
):
    path = f"v{version}/symbols/{pkg_name}/{channel}/{subdir}/{filename}"
    store.write_bytes(path, payload.metadata, payload.data)
    return True
