__all__ = (
    "ZstandardStorage",
    "CachedZstandardStorage",
    "DBZstandardStorage",
)

import fnmatch
import hashlib
import hmac
import json
import pathlib
import re
import secrets
from abc import ABC, abstractmethod, abstractproperty
from contextlib import contextmanager
from io import BytesIO

import zstandard
from fastapi.responses import StreamingResponse
from sqlalchemy import (
    JSON,
    Column,
    Integer,
    LargeBinary,
    MetaData,
    Sequence,
    String,
    Table,
    UniqueConstraint,
    create_engine,
    select,
)
from sqlalchemy.dialects.mysql import insert as mysql_insert
from sqlalchemy.dialects.postgresql import insert as psql_insert

from . import config
from .models import ReturnFormat


class AbstractStorage(ABC):
    @abstractmethod
    async def glob(self, pattern):
        raise NotImplementedError()

    async def find_latest(self, name):
        sorted_paths = sorted(
            [x async for x in self.glob(f"*/{name}")],
            key=lambda s: int(re.fullmatch(r"v(\d+)", s.split("/")[0]).groups()[0]),
            reverse=True,
        )
        if sorted_paths:
            return sorted_paths[0]
        raise KeyError(name)

    @abstractmethod
    def __contains__(self, name):
        raise NotImplementedError()

    @abstractmethod
    def write_bytes(self, name, metadata, data):
        raise NotImplementedError()

    @abstractmethod
    def _data_fh(self, name):
        raise NotImplementedError()

    def stream(self, name):
        with self._data_fh(name) as fh:
            with zstandard.ZstdDecompressor().stream_reader(fh) as reader:
                while data := reader.read(1024 * 1024):
                    yield data

    def stream_raw(self, name):
        with self._data_fh(name) as fh:
            while data := fh.read(1024 * 1024):
                yield data

    def stream_object(self, data_format: ReturnFormat, name: str):
        if data_format == ReturnFormat.JSON:
            return StreamingResponse(self.stream(name), media_type="application/json")
        if data_format == ReturnFormat.ZSTD:
            return StreamingResponse(
                self.stream_raw(name), media_type="application/zstd"
            )
        if data_format == ReturnFormat.METADATA:
            return self.metadata(name)
        raise NotImplementedError(f"Unknown data_format {data_format!r}")

    @abstractmethod
    def metadata(self, name):
        raise NotImplementedError()

    async def streaming_json_glob(self, prefix, glob_pattern):
        json_prefix = "["
        async for x in self.glob(f"{prefix}{glob_pattern}"):
            yield json_prefix
            json_prefix = ", "
            yield json.dumps(x[len(prefix) :])
        if json_prefix == "[":
            # Nothing has been returned
            yield "[]"
        else:
            yield "]"

    async def request_to_signed_bytes(self, request):
        size = 0
        signature = hmac.new(config.signature_secret, b"", hashlib.sha256)
        with BytesIO() as fh:
            with zstandard.ZstdCompressor().stream_writer(
                fh, closefd=False
            ) as compressor:
                async for chunk in request.stream():
                    size += len(chunk)
                    compressor.write(chunk)
                    signature.update(chunk)
            data = fh.getvalue()
        return signature.hexdigest(), size, data


class AbstractDBStorage(ABC):
    _insert_keys = abstractproperty()
    _metadata = abstractproperty()
    files = abstractproperty()

    def __init__(self, data_dir):
        self._data_dir = pathlib.Path(data_dir)
        self._db_url = config.db_url or f"sqlite:///{self._data_dir}/db.sqlite"
        self.engine = create_engine(self._db_url, echo=False)
        self._metadata.create_all(self.engine)

    def __repr__(self):
        return f"{self.__class__.__name__}(db_url={self._db_url!r})"

    @property
    def conn(self):
        return self.engine.connect()

    async def glob(self, pattern):
        # FIXME: This is an ugly hack...
        pattern = fnmatch.translate(pattern.replace("**", "*"))
        assert pattern.startswith(r"(?s:"), pattern
        assert pattern.endswith(r")\Z"), pattern
        pattern = pattern[len(r"(?s:") : -len(r")\Z")] + r"\Z"

        s = select([self.files.c.name]).where(self.files.c.name.regexp_match(pattern))
        for (name,) in self.conn.execute(s):
            yield name

    @property
    def _insert_stmt(self):
        if self.engine.dialect.name == "mysql":
            insert_stmt = mysql_insert(self.files)
            return insert_stmt.on_duplicate_key_update(
                **{k: getattr(insert_stmt.inserted, k) for k in self._insert_keys}
            )
        if self.engine.dialect.name == "postgresql":
            insert_stmt = psql_insert(self.files)
            return insert_stmt.on_conflict_do_update(
                constraint="name_constraint",
                set_={k: getattr(insert_stmt.excluded, k) for k in self._insert_keys},
            )
        return self.files.insert()


class ZstandardStorage(AbstractStorage):
    ext = ".json.zst"
    metadata_ext = ".json.metadata"

    def __init__(self, data_dir):
        self._data_dir = pathlib.Path(data_dir)

    def __repr__(self):
        return f"{self.__class__.__name__}(data_dir={self._data_dir!r})"

    async def glob(self, pattern):
        for fn in self._data_dir.glob(f"{pattern}{self.ext}"):
            yield self._rel(fn, self._data_dir)

    def __contains__(self, name):
        path = self._data_dir / f"{name}{self.ext}"
        return path.exists()

    def write_bytes(self, name, metadata, data):
        path = self._data_dir / f"{name}{self.ext}"
        path.parent.mkdir(parents=True, exist_ok=True)
        # Write to a temporary file to force atomic writes
        # Without this concurrent updates can cause the data to become corrupt
        path_tmp = self._data_dir / f"{name}{self.ext}.{secrets.token_urlsafe(8)}"
        # Write the actual data
        path_tmp.write_bytes(data)
        path_tmp.rename(path)
        # Write the metadata
        path_tmp.write_text(json.dumps(metadata))
        path_tmp.rename(self._data_dir / f"{name}{self.metadata_ext}")

    @contextmanager
    def _data_fh(self, name):
        path = self._data_dir / f"{name}{self.ext}"
        with path.open("rb") as fh:
            yield fh

    def metadata(self, name):
        path = self._data_dir / f"{name}{self.metadata_ext}"
        return json.loads(path.read_text())

    def _rel(self, a, b):
        return str(a.relative_to(b))[: -len(self.ext)]


class CachedZstandardStorage(AbstractDBStorage, ZstandardStorage):
    _insert_keys = ["name"]
    _metadata = MetaData()
    files = Table(
        "files",
        _metadata,
        Column("id", Integer, Sequence("user_id_seq"), primary_key=True),
        Column("name", String(512), index=True),
        UniqueConstraint("name", name="name_constraint", sqlite_on_conflict="REPLACE"),
    )

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"(data_dir={self._data_dir!r}, db_url={self._db_url!r})"
        )

    def write_bytes(self, name, metadata, data):
        super().write_bytes(name, metadata, data)
        self.conn.execute(self._insert_stmt, name=name)


class DBZstandardStorage(AbstractDBStorage, AbstractStorage):
    _insert_keys = ["name", "metadata", "data"]
    _metadata = MetaData()
    files = Table(
        "files_with_data",
        _metadata,
        Column("id", Integer, Sequence("user_id_seq"), primary_key=True),
        Column("name", String(512), index=True),
        Column("metadata", JSON),
        Column("data", LargeBinary(100 * 1024 ** 2)),
        UniqueConstraint("name", name="name_constraint", sqlite_on_conflict="REPLACE"),
    )

    def __contains__(self, name):
        return bool(
            self.conn.execute(
                select(self.files.c.name).where(self.files.c.name == name)
            ).first()
        )

    def write_bytes(self, name, metadata, data):
        self.conn.execute(self._insert_stmt, name=name, metadata=metadata, data=data)

    @contextmanager
    def _data_fh(self, name):
        (data,) = self.conn.execute(
            select([self.files.c.data]).where(self.files.c.name == name)
        ).first()
        fh = BytesIO(data)
        yield fh

    def metadata(self, name):
        (metadata,) = self.conn.execute(
            select([self.files.c.metadata]).where(self.files.c.name == name)
        ).first()
        return metadata
