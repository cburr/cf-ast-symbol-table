from fastapi import HTTPException, status

NOT_FOUND = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND, detail="Item not found"
)

INVALID_JSON = HTTPException(
    status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid JSON in request body"
)

INVALID_JSON_METADATA = HTTPException(
    status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid JSON in metadata parameter"
)

MISSING_BODY = HTTPException(
    status_code=status.HTTP_400_BAD_REQUEST, detail="Request body is empty"
)

MISSING_CREDENTIALS = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Missing authentication headers",
)

INVALID_CREDENTIALS = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
)

EXPIRED_SIGNATURE = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Signature has expired",
)
