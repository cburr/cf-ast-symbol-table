import os
import pathlib

# Required configuration
signature_secret = os.environ["ASTST_SIGNATURE_SECRET"].encode("utf-8")
data_dir = pathlib.Path(os.environ["ASTST_DATA_DIR"])
storage_class = os.environ["ASTST_STORAGE_CLASS"]

# Only used by some storage classes
db_url = os.environ.get("ASTST_STORAGE_DB_URL")
