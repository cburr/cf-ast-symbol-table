# cf-ast-symbol-table

Webapp for https://cf-ast-symbol-table.web.cern.ch/

## API documentation

* https://cf-ast-symbol-table.web.cern.ch/docs
* https://cf-ast-symbol-table.web.cern.ch/redoc

## Updating symbols

```python
import requests
import hmac
import os
import hashlib
from datetime import datetime
import time

host = "https://cf-ast-symbol-table.web.cern.ch"
secret_token = os.environ["SECRET_TOKEN"].encode("utf-8")
# Choose one of:
#     url = "/api/v0/symbol_table/example"
#     url = "/api/v0/symbols/example/conda-forge/noarch/example-0.0.0-pyh012345_0"
data = json.dumps({...})
metadata = json.dumps({...)

# Generate the signature
headers = {
    "X-Signature-Timestamp": datetime.utcnow().isoformat(),
    "X-Body-Signature": hmac.new(secret_token, data.encode(), hashlib.sha256).hexdigest(),
}
headers["X-Headers-Signature"] = hmac.new(
    secret_token,
    b"".join([
        url.encode(),
        headers["X-Signature-Timestamp"].encode(),
        headers["X-Body-Signature"].encode(),
        metadata.encode()
    ]),
    hashlib.sha256,
).hexdigest()

# Upload the data
r = requests.put(
    f"{host}{url}",
    data=data,
    headers=headers,
    params=dict(metadata=metadata),
)
r.raise_for_status()

print(f"Data is available at {host}{url}")
```

## Contributing

To create a local environment for development:

```bash
mamba env create --file environment.yaml
conda activate cf-ast-symbol-table-dev
pip install -e .[testing]
pre-commit install
```

To run the tests:

```bash
pre-commit run --all-files
pylint src/cf_ast_symbol_table
pytest . -vvv --pdb
```

To run the tests with other databases:

```bash
docker run --rm --name ast-symbol-table-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres
export ASTST_TEST_PSQL_URL="postgresql://postgres:mysecretpassword@localhost:5432/"
docker run --rm --name ast-symbol-table-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=mysecretpassword -e MYSQL_DATABASE=ast_dev -d mysql
export ASTST_TEST_MYSQL_URL="mysql://root:mysecretpassword@127.0.0.1:3306/ast_dev"
```
